﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EfixElectrical.Database;
using EfixElectrical.Models;
using EfixElectrical.Services.Interfaces;

namespace EfixElectrical.Services
{
	public class EmailService : IEmailService
	{
		private readonly EfixElectricalDbContext _db;

		public EmailService(DbContextOptions<EfixElectricalDbContext> options)
		{
			_db = new EfixElectricalDbContext(options);
		}

		public async Task SaveEmailAsync(string ip, string email)
		{
			var exist = _db.EmailSubscribe.Any(i => i.Email == email);
			if (!exist)
			{
				EmailSubscribe model = new EmailSubscribe();
				model.Email = email;
				model.IpAddress = ip;
				model.DateCreated = DateTime.Now;
				_db.EmailSubscribe.Add(model);
				await _db.SaveChangesAsync();
			}
		}
	}
}
