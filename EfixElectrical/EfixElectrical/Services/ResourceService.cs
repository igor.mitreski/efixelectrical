﻿using System.Reflection;
using Microsoft.Extensions.Localization;

namespace EfixElectrical.Services
{
    public class ResourceService
    {
        private readonly IStringLocalizer _localizer;

        public ResourceService(IStringLocalizerFactory factory)
        {
            var type = typeof(Resources.Resources);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = factory.Create("Resources", assemblyName.Name);
        }

        public LocalizedString Localize(string key)
        {
            return _localizer[key];
        }
    }

}
