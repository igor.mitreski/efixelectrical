﻿using EfixElectrical.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfixElectrical.Services.Interfaces
{
	public interface ICacheService
	{
		Task<List<EmailSubscribe>> Test();
		Task<List<MainSlider>> CacheMainSlider();
		Task<List<LedLights>> CacheLatestProducts();
		Task<List<ReferenceList>> CacheReferenceList();
		Task<List<LedLights>> CacheLedLights();
		Task<List<OurLedProducts>> CacheOurLedProducts();
		Task<List<Category>> CacheCategories();
		Task<List<Inverter>> CacheInverters();
		Task<List<LedLightImages>> CacheLedLightImages();
		Task<List<Gallery>> CacheGallery();
		Task<List<Videos>> CachePvVideos();
		Task CacheAll();

	}
}
