﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfixElectrical.Services.Interfaces
{
	public interface IEmailService
	{
		Task SaveEmailAsync(string ip, string email);
	}
}
