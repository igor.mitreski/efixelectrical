﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using EfixElectrical.Database;
using EfixElectrical.Models;
using EfixElectrical.Services.Interfaces;
using System.IO;

namespace EfixElectrical.Services
{
	public class CacheService : ICacheService
	{
        private readonly IMemoryCache _memoryCache;
        private readonly EfixElectricalDbContext _db;

        public CacheService(IMemoryCache memoryCache, DbContextOptions<EfixElectricalDbContext> options)
        {
            _memoryCache = memoryCache;
            _db = new EfixElectricalDbContext(options);
        }

        public async Task<List<EmailSubscribe>> Test()
        {
            var emailSubscribe = await _db.EmailSubscribe.ToListAsync();
            var csv = "D:\\emails.csv";
            int counter = 0;
            using (var reader = new StreamReader(csv))
            {
                
                List<string> listA = new List<string>();
                List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    int id;
                    DateTime dt;
                    if(int.TryParse(values[0], out id)) 
                    {
                        var dtParse = DateTime.TryParse(values[3], out dt);
                        var emailS = new EmailSubscribe();
                        emailS.Id = id;
                        emailS.Email = values[1];
                        emailS.IpAddress = values[2];
                        if(dtParse)
                            emailS.DateCreated = dt;

                        _db.EmailSubscribe.Add(emailS);
                        await _db.SaveChangesAsync();

                        counter++;
                        Console.WriteLine(counter);
                    }
                }
            }
            return emailSubscribe;
        }

        public async Task<List<MainSlider>> CacheMainSlider()
        {
            return await GetCache <MainSlider>();
        }

        public async Task<List<LedLights>> CacheLatestProducts()
        {
            return await GetCache<LedLights>();
        }

        public async Task<List<ReferenceList>> CacheReferenceList()
        {
            return await GetCache<ReferenceList>();
        }

        public async Task<List<LedLights>> CacheLedLights()
        {
            return await GetCache<LedLights>();
        }

        public async Task<List<OurLedProducts>> CacheOurLedProducts()
        {
            return await GetCache<OurLedProducts>();
        }

        public async Task<List<Category>> CacheCategories()
        {
            return await GetCache<Category>();
        }

        public async Task<List<Inverter>> CacheInverters()
        {
            return await GetCache<Inverter>();
        }

        public async Task<List<LedLightImages>> CacheLedLightImages()
        {
            return await GetCache <LedLightImages>();
        }

        public async Task<List<Gallery>> CacheGallery()
        {
            return await GetCache<Gallery>();
        }

        public async Task<List<Videos>> CachePvVideos()
        {
            return await GetCache<Videos>();
        }

        public async Task CacheAll()
        {
            await CacheMainSlider();
            await CacheLatestProducts();
            await CacheReferenceList();
            await CacheLedLights();
            await CacheOurLedProducts();
            await CacheCategories();
            await CacheInverters();
            await CacheLedLightImages();
            await CacheGallery();
        }

        private async Task<List<T>> GetCache<T>() where T : class
        {
            List<T> l;
            var key = typeof(T).Name;
            bool isExist = _memoryCache.TryGetValue(key, out l);
            if (!isExist)
            {
                l = await _db.Set<T>().ToListAsync();
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(30));

                _memoryCache.Set(key, l, cacheEntryOptions);
            }

            return l;
        }
    }
}
