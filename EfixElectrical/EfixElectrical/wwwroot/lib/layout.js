﻿window.fbAsyncInit = function () {
    FB.init({
        xfbml: true,
        version: 'v4.0'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-45119419-1', 'efixelectrical.com.mk');
ga('send', 'pageview');

function onEmailSubmit(token) {
    document.getElementById("email-subscribe-form").submit();
}


$(document).ready(function () {
    $(".fancybox").fancybox();

    $('a#services').click(function() {
        return false;
    });

    $('a#email-sub-btn').click(function () {
        $('form#email-subscribe-form').validate();
        if ($('form#email-subscribe-form').valid()) {
            $('form#email-subscribe-form').submit();
        }
    });

    $('a#logout').click(function() {
        $('form#logout-form').submit();
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.scrolltop').click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $('.dropdown-submenu > a').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    $('.popup').click(function (event) {
        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            url = this.href,
            opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
        window.open(url, 'share', opts);
        return false;
    });
});