﻿var yearlyPowerOnM2 = 1600.07;
var allKw = 10.000;
var referencePrice = 0.1;
var priceIncreasePercent = 5;
var dataSet = [];

$(document).ready(function () {
    $(".fancybox").fancybox();
    solarCalculatorSliders();
    solarCalculator(true);
    ledCalculator();
    $('div#calculator select').change(function () {
        ledCalculator();
    });
    var $myCarousel = $('#carousel-generic');
    var $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");

    $myCarousel.carousel({ interval: 8000 });

    doAnimations($firstAnimatingElems);

    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });

    $('.carousel[data-type="multi"] .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
        $(".fancybox").fancybox();
    });

    $('.collapse').on('shown.bs.collapse', function () {
        if ($('.navbar-header button').hasClass('collapsed')) {
            $('.exellence').show('slow');
        }
        else {
            $('.exellence').hide('slow');
        }
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        if ($('.navbar-header button').hasClass('collapsed')) {
            $('.exellence').show('slow');
        }
        else {
            $('.exellence').hide('slow');
        }
    });
});

function ledCalculator() {
    var ledPower = parseInt($('select#led-power option:selected').val());
    var ledQuantity = parseInt($('select#led-quantity option:selected').val());
    var ledTime = parseInt($('select#led-time option:selected').val());
    var fluoPower = parseInt($('select#led-power option:selected').attr('data-fluo'));
    var ledPrice = (ledPower/1000) * ledQuantity * ledTime * 6.5;
    var fluoPrice = (fluoPower / 1000) * ledQuantity * ledTime * 6.5;
    var fluoPricePercent = (fluoPrice / 100) * 20;
    var fluoPriceWithPercent = fluoPrice + fluoPricePercent;
    var priceDiff = fluoPriceWithPercent - ledPrice;
    $('#led-price').text(ledPrice.toLocaleString('mk-MK', { style: 'currency', currency: 'MKD', maximumSignificantDigits: 3 }).replace('ден', '').replace('MKD', '') + ' ' + resDenars);
    $('#fluo-power').val(resFluorescentLight + ' (' + fluoPower + 'W)');
    if (ledQuantity == 1) {
        $('#fluo-quantity').val(ledQuantity + ' ' + resLight);
    }
    else {
        $('#fluo-quantity').val(ledQuantity + ' ' + resLights);
    }
    $('#fluo-time').val(ledTime + ' ' + resHours);
    $('#fluo-price').text(fluoPriceWithPercent.toLocaleString('mk-MK', { style: 'currency', currency: 'MKD', maximumSignificantDigits: 3 }).replace('ден', '').replace('MKD', '') + ' ' + resDenars);
    $('#price-difference').text(priceDiff.toLocaleString('mk-MK', { style: 'currency', currency: 'MKD', maximumSignificantDigits: 3 }).replace('ден', '').replace('MKD', '') + ' ' + resDenars);
}

//function scroller() {
//    $("#scroller").simplyScroll({
//        customClass: 'vert',
//        orientation: 'vertical',
//        auto: true,
//        manualMode: 'loop',
//        frameRate: 10,
//        speed: 1
//    });
//}

function doAnimations(elems) {
    var animEndEv = 'webkitAnimationEnd animationend';

    elems.each(function () {
        var $this = $(this),
			$animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
            $this.removeClass($animationType);
        });
    });
}

function solarCalculatorSliders() {
    $('#installed-power').slider({
        formatter: function (value) {
            $('#kwValue').text(value);
            return resPower + ': ' + value + ' kW';
        }, 
        max: 1000
    });
    $('#installed-power').on("slide", function (slideEvt) {
        allKw = slideEvt.value;
        solarCalculator(false);
    });

    $('#price-per-kw').slider({
        formatter: function (value) {
            $('#prValue').text(value);
            return resPrice + ': ' + value + ' €';
        }
    });
    $('#price-per-kw').on("slide", function (slideEvt) {
        referencePrice = slideEvt.value;
        solarCalculator(false);
    });

    $('#year-percent').slider({
        formatter: function (value) {
            $('#peValue').text(value);
            return value + ' %';
        }
    });
    $('#year-percent').on("slide", function (slideEvt) {
        priceIncreasePercent = slideEvt.value;
        solarCalculator(false);
    });

    $('#year-av-production').slider({
        formatter: function (value) {
            $('#ypValue').text(value);
            return value + ' kW';
        }
    });
    $('#year-av-production').on("slide", function (slideEvt) {
        yearlyPowerOnM2 = slideEvt.value;
        solarCalculator(false);
    });
}

function solarCalculator(initTable) {
    if (!initTable) {
        dataSet = [];
    }
    window["refPriceYear0"] = referencePrice;
    window["refPriceYear1"] = referencePrice;

    var percents = [0.97, 0.9652, 0.9603, 0.9555, 0.9507, 0.9460, 0.9413, 0.9366, 0.9319, 0.9272, 0.9226, 0.9180, 0.9134, 0.9088, 0.9043, 0.8997, 0.8952, 0.8908, 0.8863, 0.8819, 0.8775, 0.8731, 0.8687, 0.8644, 0.8601];
    
    $.each(percents, function (i, obj) {
        var inc = i + 1;
        window["powerOnYear" + inc] = obj * yearlyPowerOnM2;
        window["pPYear" + inc] = window["powerOnYear" + inc] * allKw;
        if (inc === 1) {
            window["refPriceYear" + inc] = window["refPriceYear" + i];
        }
        else {
            window["refPriceYear" + inc] = window["refPriceYear" + i] + (window["refPriceYear" + i] / 100 * priceIncreasePercent);
        }
        window["yearlyIncomeYear" + inc] = window["pPYear" + inc] * window["refPriceYear" + inc];
        var perc = obj * 100;
        var percStr = toNumberFormat(perc);
        dataSet.push([inc, percStr + "%", toNumberFormat(window["powerOnYear" + inc]), toNumberFormat(window["pPYear" + inc]), toNumberFormat(window["refPriceYear" + inc], 3), toNumberFormat(window["yearlyIncomeYear" + inc])]);
    });

    if (initTable) {
        initializeDataTable();
    }
    else {
        reloadDataTable();
    }
}

function reloadDataTable() {
    var solarTable = $('#solar-calc-table').DataTable();
    solarTable.clear().rows.add(dataSet).draw();
}

function initializeDataTable() {
    $('#solar-calc-table').DataTable({
        data: dataSet,
        columns: [
            { title: resYear },
            { title: resEfficiency },
            { title: resOneKwYearlyProduction },
            { title: resSolarProductionPower },
            { title: resReferencePrica },
            { title: resYearlyIncome }//,
            //{ title: resYearlyProduction }
        ],
        paging: false,
        ordering: false,
        info: false,
        searching: false,
        dom: 'Bfrtip',
        buttons: ['excel', 'pdf']
    });

    $('.dt-button.buttons-excel').attr('title', 'Excel')
    $('.dt-button.buttons-pdf').attr('title', 'Pdf')
    $('.dt-button.buttons-excel').html('<i class="fa fa-file-excel-o"></i>')
    $('.dt-button.buttons-pdf').html('<i class="fa fa-file-pdf-o"></i>')
}

function toNumberFormat(number, decimals) {
    if (decimals === null || decimals === undefined) {
        decimals = 2;
    }
    return number.toLocaleString('mk-MK', { style: 'decimal', maximumFractionDigits: decimals, minimumFractionDigits: decimals });
}

function HTMLEncode(str) {
    var i = str.length,
        aRet = [];

    while (i--) {
        var iC = str[i].charCodeAt();
        if (iC < 65 || iC > 127 || (iC > 90 && iC < 97)) {
            aRet[i] = '&#' + iC + ';';
        } else {
            aRet[i] = str[i];
        }
    }
    return aRet.join('');
}
