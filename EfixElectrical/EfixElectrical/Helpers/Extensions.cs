﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace EfixElectrical.Helpers
{
	public static class Extensions
	{
        public static List<string> SplitCsv(this string csvList, bool nullOrWhitespaceInputReturnsNull = false)
        {
            if (string.IsNullOrWhiteSpace(csvList))
                return nullOrWhitespaceInputReturnsNull ? null : new List<string>();

            return csvList
                .TrimEnd(',')
                .Split(',')
                .AsEnumerable<string>()
                .Select(s => s.Trim())
                .ToList();
        }

        public static bool IsNullOrWhitespace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }

        public static string IsActive(this int category, dynamic viewBagCategoryId)
		{
            if (category == viewBagCategoryId as int?)
            {
                return "active";
            }
            else return string.Empty;
		}
    }
}
