﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using X.PagedList;
using MimeKit;
using MailKit.Net.Smtp;
using EfixElectrical.Models;
using EfixElectrical.Services.Interfaces;
using EfixElectrical.Helpers;
using EfixElectrical.Configuration;
using MailKit.Security;

namespace EfixElectrical.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICacheService _cacheService;
        private readonly IEmailService _emailService;
        private readonly AppSettings _appSettings;

        public HomeController(ICacheService cacheService,
                              IEmailService emailService,
                              IOptions<AppSettings> appSettings)
        {
            _cacheService = cacheService;
            _emailService = emailService;
            _appSettings = appSettings.Value;
        }

        public IActionResult Index()
        {
            ViewBag.ReturnUrl = "/Home/Index";
            return View();
        }

        public async Task<IActionResult> _LatestProducts()
        {
            var latestProducts = await _cacheService.CacheLatestProducts();
            return View(latestProducts);
        }

        public ActionResult EnergyLaw()
        {
            return View();
        }

        public ActionResult AboutUs()
        {
            ViewBag.ReturnUrl = "/Home/AboutUs";
            return View();
        }

        public async Task<IActionResult> Gallery(int? page, int? cat)
        {
            int pageNum = page ?? 1;
            var category = cat ?? 1;
            ViewBag.ReturnUrl = "/Home/Gallery";
            var cachedImages = await _cacheService.CacheGallery();
            var images = cachedImages.Where(i => i.Category == cat).OrderByDescending(i => i.Id).ToPagedList(pageNum, 16);
            ViewBag.Category = category;
            return View(images);
        }

        public ActionResult Lighting()
        {
            ViewBag.ReturnUrl = "/Home/Lighting";
            return View();
        }

        public async Task<IActionResult> ReferenceList()
        {
            ViewBag.ReturnUrl = "/Home/ReferenceList";
            var referenceList = await _cacheService.CacheReferenceList();
            return View(referenceList);
        }

        public async Task<IActionResult> LedLighting(int? page, int? category)
        {
            int pageNum = page ?? 1;
            ViewBag.ReturnUrl = "/Home/LedLighting";

            var cachedLedLights = await _cacheService.CacheLedLights();
            if (category != null && category != 0)
            {
                ViewBag.Category = cachedLedLights.FirstOrDefault(i => i.CategoryId == category).CategoryName;
                ViewBag.CategoryId = category;
                return View(cachedLedLights.Where(i => i.CategoryId == category).OrderByDescending(i => i.Id).ToPagedList(pageNum, 12));
            }
            else
            {
                ViewBag.Category = Resources.Resources.LedLighting_All;
                ViewBag.CategoryId = 0;
                return View(cachedLedLights.OrderByDescending(i => i.Id).ToPagedList(pageNum, 12));
            }
        }

        public async Task<ActionResult> OurLedProducts()
        {
            ViewBag.ReturnUrl = "/Home/OurLedProducts";
            var ourLedProducts = await _cacheService.CacheOurLedProducts();
            return View(ourLedProducts);
        }

        public IActionResult SolarPanels()
        {
            ViewBag.ReturnUrl = "/Home/SolarPanels";
            return View();
        }

        public async Task<IActionResult> Inverters(int? page, int? category)
        {
            var categories = await _cacheService.CacheCategories();
            var inverters = await _cacheService.CacheInverters();
            ViewBag.InverterCategories = categories.Where(i => i.ParentCategoryId == 13).ToList();

            int pageNum = page ?? 1;
            ViewBag.ReturnUrl = "/Home/Inverters";
            if (category != null && category != 0)
            {
                ViewBag.Category = categories.FirstOrDefault(i => i.Id == category).CategoryName;
                ViewBag.CategoryId = category;
                return View(inverters.Where(i => i.CategoryId == category).OrderByDescending(i => i.Id).ToPagedList(pageNum, 8));
            }
            else
            {
                ViewBag.CategoryId = 0;
            }
            return View(inverters.OrderBy(i => i.CategoryId).ToPagedList(pageNum, 8));
        }

        public async Task<IActionResult> InverterProduct(int id, string returnPage)
        {
            ViewBag.ReturnUrl = "/Home/Inverters";
            ViewBag.ReturnPage = returnPage;
            var inverters = await _cacheService.CacheInverters();
            var product = inverters.FirstOrDefault(i => i.Id == id);
            return View(product);
        }

        public async Task<IActionResult> LedProduct(int id, string returnPage)
        {
            ViewBag.ReturnUrl = "/Home/LedLighting";
            ViewBag.ReturnPage = returnPage;
            var ledLights = await _cacheService.CacheLedLights();
            var ledLightImages = await _cacheService.CacheLedLightImages();
            var product = ledLights.FirstOrDefault(i => i.Id == id);
            var characteristics = product.Property1.Split('§');
            var charDictionary = new Dictionary<string, string>();
            foreach (var c in characteristics)
            {
                var ch = c.Split(':');
                charDictionary.Add(ch[0], ch[1]);
            }
            product.Characteristics = charDictionary;
            var productImages = ledLightImages.Where(i => i.LedLightId == id);
            if (productImages != null && productImages.Any())
                product.ProductImages = productImages.ToList();

            ViewBag.MainImage = _appSettings.FullDomain + "/images/Led/" + product.Image;

            return View(product);
        }

        public ActionResult ElectricalInstallation()
        {
            ViewBag.ReturnUrl = "/Home/ElectricalInstallation";
            return View();
        }

        public async Task<IActionResult> PhotovoltaicVideos(int? page)
        {
            int pageNum = page ?? 1;
            ViewBag.ReturnUrl = "/Home/PhotovoltaicVideos";
            var cachedVideos = await _cacheService.CachePvVideos();
            var videos = cachedVideos.OrderByDescending(i => i.Id).ToPagedList(pageNum, 16);
            return View(videos);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailSubscribe(EmailSubscribeModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (ModelState.IsValid)
            {
                try
                {
                    var ipAddress = GetRequestIP(true);
                    if (ipAddress == "::1") ipAddress = "92.55.83.145";
                    await _emailService.SaveEmailAsync(ipAddress, model.Email);

                    await SendSmtpMessage(model.Email);
                    TempData["EmailSubMsgSuccess"] = Resources.Resources.EmailSubMsgSuccess;
                    return Redirect(returnUrl);
                }
                catch
                {
                    TempData["EmailSubMsgError"] = Resources.Resources.EmailSubMsgЕrror;
                    return Redirect(returnUrl);
                }
            }
            TempData["EmailSubMsgError"] = Resources.Resources.EmailSubMsgЕrror;
            return Redirect(returnUrl);
        }

        public ActionResult Error()
        {
            ViewBag.ReturnUrl = "/Home/Error";
            return View();
        }

        public ActionResult Terms()
        {
            ViewBag.ReturnUrl = "/Home/Terms";
            return View();
        }

        public ActionResult Privacy()
        {
            ViewBag.ReturnUrl = "/Home/Privacy";
            return View();
        }

        [HttpGet]
        public IActionResult ChangeCulture(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddMonths(1) }
            );

            return LocalRedirect(returnUrl);
        }

        protected ViewResult Success<TModel>(TModel viewModel)
        {
            ViewData["success"] = Resources.Resources.Contact_SendSuccessfully;
            return View(viewModel);
        }

        public ViewResult Contact()
        {
            ViewBag.ReturnUrl = "/Home/Contact";
            return View();
        }

        public ActionResult Catalogue()
        {
            ViewBag.ReturnUrl = "/Home/Catalogue";
            return View();
        }

        public ActionResult Catalogues(string name)
        {
            ViewBag.ReturnUrl = "/Home/Catalogues";
            ViewBag.Name = name.ToLower();
            int pages = 0;
            if (name.ToLower() == "homeled")
                pages = 33;
            else if (name.ToLower() == "efixcandelabrum")
                pages = 251;
            else if (name.ToLower() == "efixmetal1")
                pages = 92;
            else if (name.ToLower() == "efixmetal2")
                pages = 3;
            else if (name.ToLower() == "scene")
                pages = 43;

            return View(new KeyValuePair<int, string>(pages, name.ToLower()));
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(ContactView view)
        {
            ViewBag.ReturnUrl = "/Home/Contact";
            if (!ModelState.IsValid)
            {
                TempData["errormessage"] = Resources.Resources.Contact_AllFieldsRequired;
                return View(view);
            }
            try
            {
                await SendSmtpMessage(view.Email, view.Name + " " + view.Subject, view.Message);
                TempData["successmessage"] = Resources.Resources.Contact_SendSuccessfully;
                return View();
            }
            catch (Exception e)
            {
                TempData["errormessage"] = Resources.Resources.Contact_CannotConnect;
                return View(view);
            }
        }

        public string GetRequestIP(bool tryUseXForwardHeader = true)
        {
            string ip = null;

            // todo support new "Forwarded" header (2014) https://en.wikipedia.org/wiki/X-Forwarded-For

            // X-Forwarded-For (csv list):  Using the First entry in the list seems to work
            // for 99% of cases however it has been suggested that a better (although tedious)
            // approach might be to read each IP from right to left and use the first public IP.
            // http://stackoverflow.com/a/43554000/538763
            //
            if (tryUseXForwardHeader)
                ip = GetHeaderValueAs<string>("X-Forwarded-For").SplitCsv().FirstOrDefault();

            // RemoteIpAddress is always null in DNX RC1 Update1 (bug).
            if (ip.IsNullOrWhitespace() && Request.HttpContext?.Connection?.RemoteIpAddress != null)
                ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();

            if (ip.IsNullOrWhitespace())
                ip = GetHeaderValueAs<string>("REMOTE_ADDR");

            // _httpContextAccessor.HttpContext?.Request?.Host this is the local host.

            if (ip.IsNullOrWhitespace())
                throw new Exception("Unable to determine caller's IP.");

            return ip;
        }

        public T GetHeaderValueAs<T>(string headerName)
        {
            StringValues values = new StringValues();

            if (Request?.Headers?.TryGetValue(headerName, out values) ?? false)
            {
                string rawValues = values.ToString();   // writes out as Csv when there are multiple.

                if (!rawValues.IsNullOrWhitespace())
                    return (T)Convert.ChangeType(values.ToString(), typeof(T));
            }
            return default(T);
        }

        private async Task SendSmtpMessage(string email, string subject = null, string body = null)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_appSettings.SmtpEmail, _appSettings.SmtpEmail));
            emailMessage.To.Add(new MailboxAddress(email, email));

            var bccEmails = _appSettings.SmtpBcc.Split(',');
            foreach (var bcc in bccEmails)
            {
                emailMessage.Bcc.Add(new MailboxAddress(bcc, bcc));
            }
            if (string.IsNullOrEmpty(subject))
                emailMessage.Subject = Resources.Resources.EmailSubMsgSuccess;
            else
                emailMessage.Subject = subject;


            if (string.IsNullOrEmpty(body))
                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = string.Format("<img src=\"https://www.efixelectrical.com.mk/images/logo.png\" /><br/><br/><strong>" + email + "</strong><br/><br/>" + Resources.Resources.EmailBodySuccessfullyRegistered) };
            else
                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = body };

            await SendAsync(emailMessage);

        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_appSettings.SmtpHost, _appSettings.SmtpPort, SecureSocketOptions.Auto);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_appSettings.SmtpEmail, _appSettings.SmtpPassword);
                    await client.SendAsync(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception, or both.
                    throw;
                }
                finally
                {
                    await client.DisconnectAsync(true);
                    client.Dispose();
                }
            }
        }
    }
}


