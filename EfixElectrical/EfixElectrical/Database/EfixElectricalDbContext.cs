﻿using EfixElectrical.Models;
using Microsoft.EntityFrameworkCore;

namespace EfixElectrical.Database
{
	public partial class EfixElectricalDbContext : DbContext
	{
        public DbSet<LedLights> LedLights { get; set; }
        public DbSet<EmailSubscribe> EmailSubscribe { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<OurLedProducts> OurLedProducts { get; set; }
        public DbSet<Category> Categories1 { get; set; }
        public DbSet<Inverter> Inverters { get; set; }
        public DbSet<MainSlider> MainSlider { get; set; }
        public DbSet<ReferenceList> ReferenceList { get; set; }
        public DbSet<LedLightImages> LedLightImages { get; set; }
        public DbSet<Videos> Videos { get; set; }

        #region Properties

        public EfixElectricalDbContext(DbContextOptions<EfixElectricalDbContext> options)
            : base(options)
        {
        }

        #endregion

        #region Protected Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EfixElectricalDbContext).Assembly);
        }

        #endregion

    }
}
