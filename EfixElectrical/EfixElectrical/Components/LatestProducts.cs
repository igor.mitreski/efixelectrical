﻿using EfixElectrical.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfixElectrical.Components
{
	public class LatestProducts : ViewComponent
	{
		private readonly ICacheService _cacheService;

		public LatestProducts(ICacheService cacheService)
		{
			_cacheService = cacheService;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var latestProducts = await _cacheService.CacheLatestProducts();
			return View(latestProducts);
		}

	}
}
