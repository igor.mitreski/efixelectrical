﻿using EfixElectrical.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfixElectrical.Components
{
	public class SolarCarousel : ViewComponent
	{
		private readonly ICacheService _cacheService;

		public SolarCarousel(ICacheService cacheService)
		{
			_cacheService = cacheService;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var cachedImages = await _cacheService.CacheGallery();
			var images = cachedImages.Where(i => i.Category == 2).OrderByDescending(i => i.Id).Take(12).ToList();
			return View(images);
		}
	}
}
