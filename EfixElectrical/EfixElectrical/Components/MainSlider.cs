﻿using EfixElectrical.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EfixElectrical.Components
{
	public class MainSlider : ViewComponent
	{
		private readonly ICacheService _cacheService;

		public MainSlider(ICacheService cacheService)
		{
			_cacheService = cacheService;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var slider = await _cacheService.CacheMainSlider();

			return View(slider);
		}

	}
}
