using System.Globalization;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using EfixElectrical.Database;
using EfixElectrical.Services;
using EfixElectrical.Services.Interfaces;
using EfixElectrical.Configuration;
using Imageflow.Server;
using System.IO;

namespace EfixElectrical
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<ResourceService>();
			services.AddLocalization(options => options.ResourcesPath = "Resources");

			services.Configure<RequestLocalizationOptions>(options =>
			{
				var supportedCultures = new[]
				{
					new CultureInfo("mk"),
					new CultureInfo("en")
				};
				options.DefaultRequestCulture = new RequestCulture(culture: "mk", uiCulture: "mk");
				options.SupportedCultures = supportedCultures;
				options.SupportedUICultures = supportedCultures;
			});

			services.AddMvc()
				.AddViewLocalization()
				.AddDataAnnotationsLocalization(options =>
				{
					options.DataAnnotationLocalizerProvider = (type, factory) =>
					{
						var assemblyName = new AssemblyName(typeof(Resources.Resources).GetTypeInfo().Assembly.FullName);
						return factory.Create("Resources", assemblyName.Name);
					};
				});

			services.AddDbContext<EfixElectricalDbContext>(options => options.UseMySQL(Configuration.GetConnectionString("DefaultConnection")));

			services.AddHttpContextAccessor();

			services.AddControllersWithViews();
			services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
			services.AddTransient<ICacheService, CacheService>();
			services.AddTransient<IEmailService, EmailService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseImageflow(new ImageflowMiddlewareOptions()
				// Maps / to WebRootPath
				.SetMapWebRoot(true)
				.MapPath("/folder", Path.Combine(env.ContentRootPath, "folder")));

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			
			var supportedCultures = new[] { "mk", "en" };
			var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
				.AddSupportedCultures(supportedCultures)
				.AddSupportedUICultures(supportedCultures);

			app.UseRequestLocalization(localizationOptions);

			app.UseRouting();

			app.UseCors(x => x
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader());

			app.UseAuthorization();
			

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
