﻿namespace EfixElectrical.Configuration
{
	public class AppSettings
	{
		public string FullDomain { get; set; }

		public string SmtpHost { get; set; }

		public int SmtpPort { get; set; }

		public bool SmtpSsl { get; set; }

		public string SmtpEmail { get; set; }

		public string SmtpPassword { get; set; }

		public string SmtpBcc { get; set; }
	}
}
