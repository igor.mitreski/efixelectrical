﻿using System.ComponentModel.DataAnnotations;

namespace EfixElectrical.Models
{
    public class ValidateEmailAttribute : RegularExpressionAttribute
    {
        public ValidateEmailAttribute()
            : base(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        {
        }
    }
}
