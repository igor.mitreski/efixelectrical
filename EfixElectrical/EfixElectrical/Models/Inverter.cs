using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{    
    [Table("Inverters")]
    public class Inverter : MainEntity
    {
        public string Model { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
    
        public virtual Category Categories { get; set; }
    }
}
