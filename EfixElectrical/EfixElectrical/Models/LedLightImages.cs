using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{  
    [Table("LedLightImages")]
    public class LedLightImages : MainEntity
    {
        public string ImageName { get; set; }
        public int LedLightId { get; set; }
        public string Title { get; set; }
    }
}
