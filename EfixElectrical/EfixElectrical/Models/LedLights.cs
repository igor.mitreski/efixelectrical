using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{    
    [Table("LedLights")]
    public class LedLights : MainEntity
    {
        public string Image { get; set; }
        public string Model { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Property1 { get; set; }
        public int Quantity { get; set; }
        public int Import { get; set; }
        public int Export { get; set; }

        [NotMapped]
        public Dictionary<string, string> Characteristics { get; set; }

        [NotMapped]
        public List<LedLightImages> ProductImages { get; set; }

        public virtual Category Category { get; set; }
    }
}
