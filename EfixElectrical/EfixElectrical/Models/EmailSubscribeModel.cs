﻿using System.ComponentModel.DataAnnotations;

namespace EfixElectrical.Models
{
    public class EmailSubscribeModel
    {
        [Required(ErrorMessageResourceName = "Contact_EmailRequired", ErrorMessageResourceType = typeof(Resources.Resources))]
        [EmailAddress(ErrorMessageResourceName = "Contact_ValidEmail", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessage = null)]
        public string Email { get; set; }
    }
}
