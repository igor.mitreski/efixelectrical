using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("Gallery")]
    public class Gallery : MainEntity
    {
        public string Name { get; set; }
        public string ImageName { get; set; }
        public int? Category { get; set; }
    }
}
