using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("ReferenceList")]
    public class ReferenceList : MainEntity
    {
        public string Name_MK { get; set; }
        public string Industry_MK { get; set; }
        public string City_MK { get; set; }
        public string Service_MK { get; set; }
        public string Name_EN { get; set; }
        public string Industry_EN { get; set; }
        public string City_EN { get; set; }
        public string Service_EN { get; set; }
        public string Link { get; set; }
        public Nullable<int> Order { get; set; }
    }
}
