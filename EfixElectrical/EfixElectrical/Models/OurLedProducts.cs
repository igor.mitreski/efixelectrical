using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("OurLedProducts")]
    public class OurLedProducts : MainEntity
    {
        public string Name { get; set; }
        public string ImageName { get; set; }
    }
}
