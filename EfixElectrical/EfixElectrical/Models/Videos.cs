using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("Videos")]
    public class Videos : MainEntity
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
    }
}
