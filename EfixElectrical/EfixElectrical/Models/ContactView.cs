﻿using System.ComponentModel.DataAnnotations;

namespace EfixElectrical.Models
{
    public class ContactView
    {
        [Required(ErrorMessageResourceName = "Contact_NameRequired", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Name { get; set; }
        [Required(ErrorMessageResourceName = "Contact_EmailRequired", ErrorMessageResourceType = typeof(Resources.Resources)), ValidateEmail(ErrorMessageResourceName = "Contact_ValidEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }
        [Required(ErrorMessageResourceName = "Contact_SubjectRequired", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Subject { get; set; }
        [Required(ErrorMessageResourceName = "Contact_MessageRequired", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Message { get; set; }
    }
}
