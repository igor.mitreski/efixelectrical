using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{    
    [Table("MainSlider")]
    public class MainSlider : MainEntity
    {
        public string ImageName { get; set; }
        public string Title_MK { get; set; }
        public string Description_MK { get; set; }
        public string Title_EN { get; set; }
        public string Description_EN { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
    }
}
