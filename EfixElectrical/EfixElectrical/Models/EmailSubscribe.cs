using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{  
    [Table("EmailSubscribe")]
    public class EmailSubscribe : MainEntity
    {
        public string Email { get; set; }
        public string IpAddress { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
