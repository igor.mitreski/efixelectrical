using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("Categories")]
    public class Category : MainEntity
    {
        public Category()
        {
            this.Inverters = new HashSet<Inverter>();
            this.LedLights = new HashSet<LedLights>();
        }
    
        public string CategoryName { get; set; }
        public Nullable<int> ParentCategoryId { get; set; }
    
        public virtual ICollection<Inverter> Inverters { get; set; }
        public virtual ICollection<LedLights> LedLights { get; set; }
    }
}
