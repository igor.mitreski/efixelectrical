using System.ComponentModel.DataAnnotations.Schema;

namespace EfixElectrical.Models
{
    [Table("Pricelists")]
    public class Pricelists : MainEntity
    {
        public string EmbeddingUrl { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
    }
}
